# aws s3 sync lambda function

## Getting started with IAM permissions. 

Create iam role with following policies:
```
1. AmazonS3FullAccess
2. AWSLambdaBasicExecutionRole
```

## Upload Lambda layer

- [ ] Open AWS Lambda service from UI console. This should get you the column on the left with al options and additional resources. If not hit the hamburger you should see the column. 
![](./layer.png) 
![](./create-layer.png)
- [ ] Create a function with following code.

```py
import subprocess
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def run_command(command):
    command_list = command.split(' ')

    try:
        logger.info("Running shell command: \"{}\"".format(command))
        result = subprocess.run(command_list, stdout=subprocess.PIPE);
        logger.info("Command output:\n---\n{}\n---".format(result.stdout.decode('UTF-8')))
    except Exception as e:
        logger.error("Exception: {}".format(e))
        return False

    return True

def lambda_handler(event, context):
    run_command('/opt/lambdaclilayer/aws s3 sync --delete s3://source-bucket/ s3://destination-bucket')
```
- [ ] Make sure to check the run time environment to Python 3.8 
- [ ] Use execution role that was created in the first step. 
- [ ] Make sure to change the Memory to 1024 MB and timeout to 2 mins.This is to avoid any command timeouts by lambda. Bigger the files, better to change the timeout to higher numbers. 
![](./general_configurations.png)


## Trigger a test event
[ ] Trigger a test event if this fails with permission issue or file doesn't exist, be sure to change the run_command to see through the directory structure. 
run_command('ls -ltr /opt')

**NOTE** 
/opt is the default filesystem for all the the layers created. 

```py
import subprocess
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def run_command(command):
    command_list = command.split(' ')

    try:
        logger.info("Running shell command: \"{}\"".format(command))
        result = subprocess.run(command_list, stdout=subprocess.PIPE);
        logger.info("Command output:\n---\n{}\n---".format(result.stdout.decode('UTF-8')))
    except Exception as e:
        logger.error("Exception: {}".format(e))
        return False

    return True

def lambda_handler(event, context):
    run_command('ls -ltr /opt')
```
